package com.array;

import java.util.Arrays;

public class Two_Dimension_Array {

	public static void main(String[] args) {
		int[][] StudentMarks = new int[4][3];

		StudentMarks[0][0] = 67; 
		StudentMarks[0][1] = 58; 
		StudentMarks[0][2] = 89;

		StudentMarks[1][0] = 50; 
		StudentMarks[1][1] = 60; 
		StudentMarks[1][2] = 45; 

		StudentMarks[2][0] = 97; 
		StudentMarks[2][1] = 56; 
		StudentMarks[2][2] = 78; 

		StudentMarks[3][0] = 49; 
		StudentMarks[3][1] = 78; 
		StudentMarks[3][2] = 95; 

		System.out.println("Student Marks Matrix");
		System.out.println(Arrays.deepToString(StudentMarks));

	}

}
