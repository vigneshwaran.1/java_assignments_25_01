package com.collections;

import java.util.ArrayList;
import java.util.Iterator;

public class ArrayListLoop {
	
	public static void main(String[] args) {
		
		ArrayList<Integer>l1=new ArrayList<Integer>();
		
		l1.add(3);
		l1.add(35);
		l1.add(40);
		l1.add(5);
		l1.add(8);
		
		System.out.println("Using Advanced for loop");
		
		for (Integer n:l1) {
			
			System.out.println(n);
			
		}
	}

}
